import express from "express";
import { getProd,getsingleProd,addProd,updateProd,deleteProd } from "../controller/prodController.js";
const prodRoute = express.Router();

prodRoute.get('/getprod', getProd);
prodRoute.get('/:id', getsingleProd);
prodRoute.post('/addprod', addProd);
prodRoute.put('/:id', updateProd);
prodRoute.delete('/:id', deleteProd);


export default prodRoute;