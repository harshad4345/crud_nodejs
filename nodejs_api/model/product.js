import mongoose from 'mongoose';

const prodSchema = mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    prodName:String,
    Description:String,
    price:Number,
});

const Product = mongoose.model('product', prodSchema);

export default Product;