import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    name:String,
    email:String,
    phone:Number,
    username:String,
    password:String,
    date:{
        type:Date,
        default: Date.now,
    }
},{timestamp:true});

const User = mongoose.model('user', userSchema);

export default User;