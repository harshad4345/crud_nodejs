import Product from "../model/product.js";
import mongoose from "mongoose";

// For Retrive Product List
export const getProd = (req, res, nexr) => {
  Product.find()
    .then((result) => {
      res.status(200).json({
        ProductList: result,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};

// Get Product By ID
export const getsingleProd = (req, res, nexr) => {
  // console.log(req.params.id)
  Product.findById(req.params.id)
    .then((result) => {
      res.status(200).json({
        Product: result,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};

// For Add Product
export const addProd = (req, res, nexr) => {
  // Form Validation
  if (!req.body.Product_Name || !req.body.Description || !req.body.Price) {
    res.status(400).json({
      msg: "Input Filed Can Not Be Empty",
    });
  }
  // Form Validation Exit
  else {
    //   Create Product
    const product = new Product({
      _id: new mongoose.Types.ObjectId(),
      prodName: req.body.Product_Name,
      Description: req.body.Description,
      price: req.body.Price,
    });
    // Then Save
    product
      .save()
      .then((result) => {
        res.status(200).json({
          newProd: result,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err.message,
        });
      });
  }
};

// For Update Product
export const updateProd = (req, res, next) => {
    // console.log(req.params.id)
  Product.findOneAndUpdate({ _id: req.params.id },
    {
      $set: {
        prodName: req.body.Product_Name,
        Description: req.body.Description,
        price: req.body.Price,
      },
    })
      .then((result) => {
        res.status(200).json({
          updateProd: result,
        });
      })
      .catch((err) => {
        res.status(500).json({
          error: err.message,
        });
      });
};

// For Delete Product
export const deleteProd = (req, res, nexr) => {
  Product.deleteOne({ _id: req.params.id })
    .then((result) => {
      res.status(200).json({
        message: "Product Deleted",
        result: result,
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      });
    });
};
