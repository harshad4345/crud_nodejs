import User from "../model/user.js";
import mongoose from 'mongoose';
import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';

// for Registration
export const register = (req, res, next)=>{
  // Form validation
  if(!req.body.name || !req.body.email || !req.body.phone || !req.body.username){
    res.status(400).json({
      msg:"Input Filed Can Not Be Empty"
    })
  }
  else if(req.body.password.length < 8){
    res.status(400).json({
      msg:"Password Must Be 8 Characters"
    })
  }
  // Form Validation Exit
  else{
  // First Hash Password 
    bcrypt.hash(req.body.password,10,(err,hash)=>{
    if(err){
      return res.status(500).json({
        error:err.message
      })
    }
    // Then create New User
    else{
      const user = new User({
        _id:new mongoose.Types.ObjectId,
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        username:req.body.username,
        password:hash,
      })
      // Then Save
      user.save()
      .then(result=>{
        res.status(200).json({
          new_user:result
        })
      })
      .catch(err=>{
        res.status(500).json({
          error:err.message
        })
      })
    }
  })
}
}

// for Login
export const login = (req,res,next)=>{
  // User Check
  User.find({username:req.body.username}).exec()
  .then(user=>{
    // User Check
    if(user.length < 1){
      return res.status(400).json({
        msg:"User Not Exist"
      })
    }
    // if User Exist Then Compare password Using bcrypt
    bcrypt.compare(req.body.password,user[0].password,(err,result)=>{
      // if password Not Match
      if(!result){
        return res.status(401).json({
          msg:"Invalid Credentials"
        })
      }
      // If password Match Then Create Token For Future Auth
      if(result){
        const token = jwt.sign({
          username:user[0].username,
          name:user[0].name,
          email:user[0].email,
          phone:user[0].phone,
        },"This Is sample Text",
        {
          // Token expire Time
         expiresIn:"24h" 
        });
        // Retun Response
        res.status(200).json({
          username:user[0].username,
          name:user[0].name,
          email:user[0].email,
          phone:user[0].phone,
          token:token,
        })
      }
    })
  })
  .catch(err=>{
    res.status(500).json({
      error:err.message
    })
  })
}