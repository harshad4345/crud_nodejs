import express from "express";
import router from "./Router/userRoute.js";
import prodRoute from "./Router/prodRoute.js";
import mongoose from "mongoose";
import cors from "cors";
import bodyParser from "body-parser";

const app = express();

app.use(bodyParser.json({extended:true}));
app.use(bodyParser.urlencoded({extended:true}));

app.use(cors());

// Route
app.use('/users', router);
app.use('/product', prodRoute);

// For Invalid URL
app.use((req,res,next)=>{
  res.status(404).json({
    error:"Url Not Found"
  })
})

// For mongodb database
const PORT = 8080;
const URL =
  "mongodb+srv://root:root@authuser.qid7s.mongodb.net/AuthUser?retryWrites=true&w=majority"

mongoose
  .connect(URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    app.listen(PORT);
    console.log("Server Running on port:"+PORT);
  })
  .catch((error) => {
    console.log("Error:", error.message);
  });
